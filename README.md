# Chatbot-trial
Chatbot that will respond with the Shakespeare sentence from Hamlet that is most similar to the user input 
(similar according to cosine similarity)

author: Ela Elsholz

date: June 2019

## Requirements
Docker (Desktop) installed, Development Version: 18.09.2

when run without Docker:

[Chatterbot](https://chatterbot.readthedocs.io/en/stable/)
```pip install chatterbot```
Chatterbot corpus 
```pip install chatterbot-corpus```

Pandas ```pip install pandas```


for the elg-mq version without docker
```pip install pika```

## Usage
```
docker build -t shakespearebot your-file-path
docker run -ti shakespearebot
```