# author: Ela Elsholz
# date: June 2019
# description: chitter-chatter chatbot responds with 'most similar' Shakespeare quotes from Hamlet
#                      (similar according to cosine similarity)
# realized using python chatbot framework chatterbot:
# https://chatterbot.readthedocs.io/en/stable/, accessed 18.08.2018

from chatterbot import ChatBot
from chatterbot.trainers import ListTrainer
import pandas as pd

chatbot = ChatBot("Eddie")

def train():
    corpus = pd.read_csv('corpora/hamlet.csv', sep='\t')
    trainer = ListTrainer(chatbot)
    trainer.train(corpus["original"])

# run bot
def create_shakespeare_response(request):
    response = chatbot.get_response(request)
    return response

